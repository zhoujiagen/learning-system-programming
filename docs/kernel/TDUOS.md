# Notes of **The Design of the UNIX Operating System**

|时间       |内容      |
|:---       |:---      |
|2022-07-21 |kick off.<br/> Skimming chapter 1-5. |
|2022-08-01 |add behaviour notes in chapter 1-5.  |

## Tips for Recapture

<!-- 帮助重温的过程总结. -->

1. Step 1
2. Step 2
3. Step 3
4. Step 4

## 术语

<!-- 记录阅读过程中出现的关键字及其简单的解释. -->

<!-- 进展中需要再次确认的术语:

进行中: 术语1
已完成: ~~术语1~~
-->

## 介绍

<!-- 描述书籍阐述观点的来源、拟解决的关键性问题和采用的方法论等. -->

## 动机

<!-- 描述阅读书籍的动机, 要达到什么目的等. -->

1. 了解UNIX系统中基本的数据结构和算法.

## 概念结构

<!-- 描述书籍的行文结构, 核心主题和子主题的内容结构和关系. -->

### 1. General Overview of the System
#### 1.1 History
#### 1.2 System Structure

1. Hardware
2. System Kernel
3. Programs: interact with the kernel by invoking system calls

#### 1.3 User Perspective

1. The File System
2. Processing Environment
3. Building Block Primitives: the capability to redirect I/O, pipe

#### 1.4 Operating System Services

1. Controlling the execution of processes by allowing their creation, termination or suspension, and communication.
2. Scheduling processes fairly for execution on the CPU.
3. Allocating main memory for an executing process.
4. Allocating secondary memory for efficient storage and retrieval of user data.
5. Allowing processes controoled access to peripheral devices such as terminals, tape drivers, disk drivers, and network devices.

#### 1.5 Assumptions About Hardware

When a process exeecutes a system call, the **execution mode** of the process changes from **user mode** to **kernel mode**: the operating system executes and attempts to service the user request, returning an error code if it fails.

Even if the user makes no explicit requests for operating system services, the operating system still does *bookkeeping operations that relate to the user process*, *handling interrupts*, *scheduling processes*, *managing memory*, and so on.

#### 1.6 Summary

### 2. Introduction to the Kernel
#### 2.1 Architecture of the UNIX Operating System

Figure 2.1. Block Diagram of the System Kernel

#### 2.2 Introduction to System Concepts

1. An overview of the file subsystem
2. Processes: context, states, state transitions, sleep and wakeup

#### 2.3 Kernel Data Structures

Most kernel data structures occpupy fixed-size tables rather than dynamically allocated space.

Algoriths typically use simple loops to find free table entries, a method that is easier to understand and sometimes more efficient than more complicated allocation schemes.

#### 2.4 System Administration

Administrative processes are loosely classified as those process taht do various functions for the general welfare of the user community, including *disk formating*, *creation of new file systems*, *repair of damaged file systems*, *kernel debugging*, and others.

#### 2.5 Summary and Preview

### 3. The Buffer Cache
#### 3.1 Buffer Headers
#### 3.2 Structure of the Buffer Pool

#### 3.3 Scenarios for Retrieval of a Buffer

#### 3.4 Reading and Writing Disk Blocks

```c
// algorithm for buffer allocation
getblk(file system number, 
    block number)
    : locked buffer that can now be used for block

// algorithm for releasing a buffer
brelse(locked buffer)
    : none

// algorithm for reading a disk block
bread(file system block number)
    : buffer containing data

// algorithm for block read ahread
breada(file system block number for immediate read,
    file system block number for asynchronous read)
    : buffer containing data for immediate read

// algorithm for writing a disk block
bwrite(buffer)
    : none
```
#### 3.5 Advantages and Disadvantages of the Buffer Cache
#### 3.6 Summary

### 4. Internal Representation of Files
#### 4.1 Inodes

```c
// algorithm for allocation of in-code inodes
iget(file system inode number)
    : locked inode

// releasing an inode
iput(pointer to in-core inode)
    : none
```

#### 4.2 Structure of a Regular File

```c
// block map of logical file byte offset to file system block
bmap(inode,
    byte offset)
    : block number in file system,
      byte offset into block
      bytes of I/O in block
      read ahead block number
```

#### 4.3 Directories
#### 4.4 Conversion of a Path Name to an Inode

```c
// algorithm for conversion of a path name to an inode
namei(path name)
    : locked inode
```

#### 4.5 Super Block
#### 4.6 Inode Assignment to a New File

```c
// algorithm for assigning new inodes
ialloc(file system)
    : locked inode

// algorithm for freeing inode
ifree(file system inode number)
    : none
```

#### 4.7 Allocation of Disk Blocks

```c
// algorithm for allocating disk block
alloc(file system number)
    : buffer for new block
```

#### 4.8 Other File Types
#### 4.9 Summary

### 5. System Calls for the File System
#### 5.1 Open

```c
// algorithm for opening a file
open(file name,
    type of open,
    file permission for creation type of open)
    : file descriptor
```

#### 5.2 Read

```c
// algorithm for reading a file
read(user file descriptor,
    address of buffer in user process,
    number of bytes to read)
    : count of bytes copied into user space
```

#### 5.3 Write

```c
number = write(fd, buffer, count);
```

#### 5.4 File and Record Locking

**File locking** is the capability to prevent other process from reading or writing any part of an entire file, and **record locking** is the capability to prevent other processes from reading or writing particular records (parts of a file between particular byte offsets).

#### 5.5 Adjusting the Position of File I/O — lseek

```c
position = lseek(fd, offset, reference);
```

#### 5.6 Close

```c
close(fd);
```

#### 5.7 File Creation

```c
// algorithm for creating a file
creat(file name,
    permission settings)
    : file descriptor
```

#### 5.8 Creation of Special Files

The system call `mknod` creates special files in the system, including named pipes, device files, and directories.

```c
// algorithm for making new node
mknod(node or file name,
    file type,
    permissions,
    major, minor device number for block, character special files)
    : none
```

#### 5.9 Change Directory and Change Root

```c
// algorithm for changing current directory
chdir(new directory name)
    : none

// process can change their notion of the file system root via the chroot system call
chroot(pathname);
```

#### 5.10 Change Owner and Change Mode

```c
chown(pathname, owner, group);
chmod(pathname, mode);
```

#### 5.11 Stat and fstat

```c
stat(pathname, statbuffer);
fstat(fd, statbuffer);
```

#### 5.12 Pipes

**Pipes** allow transfer of data between processes in a first-in-first-out manner (FIFO), and they also allow synchronization of process execution.

```c
// algorithm for creating of (unnamed) pipes
pipe(none)
    : read file descriptor,
      write file descriptor

open
read
write
close
```

#### 5.13 Dup

The `dup` system call copies a file descriptor into the first free slot of the user file descriptor table, returning the new file descriptor to the user.

```c
newfd = dup(fd);
```

#### 5.14 Mounting and Unmounting File Systems

```c
// algorithm for mounting a file system
mount(file name of block special file,
    firectory name of mount point,
    options)
    : none

// algorithm for unmounting a file system
unmount(special file name of file system to be unmounted)
    : none
```

#### 5.15 Link

```c
// algorithm for linking files
link(existing file name,
    new file name)
    : none
```

#### 5.16 Unlink

```c
// algorithm for unlinking a file
unlink(file name)
    : none
```

#### 5.17 File System Abstractions

file system types

#### 5.18 File System Maintenance

The command `fsck` checks for inconsistencies in file system and repairs the file system if necessary.
It accesses the file system by its block or raw interface, and bypasses the regular file access methods.

examples of inconsistencies:

- a disk block may belong to more that one inode or to the list of free blocks and an inode.
- a block number is not on the free list of blocks nor contained in a file.
- an inode may have non-0 link count, but its inode number may not exist in any directories in the file system.
- The format of an inode is incorrect.
- An inode number appears in a directory entry, but the inode is free.
- The number of free blocks or free inodes recorded in the super block does not conform to the number that exist on disk.

#### 5.19 Summary

### 6. The Structure of Processes
#### 6.1 Process States and Transitions
#### 6.2 Layout of System Memory
#### 6.3 The Context of a Process
#### 6.4 Saving the Context of a Process
#### 6.5 Manipulation of the Process Address Space
#### 6.6 Sleep
#### 6.7 Summary

### 7. Process Control
#### 7.1 Process Creation
#### 7.2 Signals
#### 7.3 Process Termination
#### 7.4 Awaiting Process Termination
#### 7.5 Invoking Other Programs
#### 7.6 The User ID of a Process
#### 7.7 Changing the Size of a Process
#### 7.8 The Shell
#### 7.9 System Boot and the INIT Process
#### 7.10 Summary

### 8. Process Scheduling and Time
#### 8.1 Process Scheduling
#### 8.2 System Calls For Time
#### 8.3 Clock
#### 8.4 Summary

### 9. Memory Management Policies
#### 9.1 Swapping
#### 9.2 Demand Paging
#### 9.3 A Hybrid System With Swapping and Demand Paging
#### 9.4 Summary

### 10. The I/O Subsystem

The **I/O subsystem** allows  a process to communicate with peripheral devices such as disks, tape drivers, terminals, printers, and networks, and the kernel modules that control devices are known as **device drivers**.

There is usually a one-to-one correspondence between device drivers and device types: systems may contain

- one disk diver to control all disk drivers,
- one terminal diver to controll all terminals,
- one tape driver to controll all tape divers.

The system supports *Software devices*, which have no associated physical device.

#### 10.1 Driver Interfaces

The UNIX system contains two types of devices: 

- **block** devices: suck as disks and tapes, look like random access storage devices to the rest of the system.
- **raw** or **character** devices: include all other devices such as terminals and netwrok media.

Every device has a name that looks like a file name and is accessed like a file.
The device special file has an inode and occupies a node in the directory hierarchy of the file system.
The device file is distinguished from other files by *the file type* storead in ints inode, either *block* or *character special*.

##### 10.1.1 System Configuration

The kernel to driver interface is described by the **block device switch table** and the **character device switch table**.
Each deivce type has entries in the table taht direct the kernel to the appropriate driver interfaces for the system calls.

Figure 10.2. Sample Block and Character Device Switch Tables

block device swith table

|entry |open   |close   |strategy   |
|:---  |---:   |---:    |---:       |
|0     |gdopen |gdclose |gdstrategy |
|1     |gtopen |gtclose |gtstrategy |


character device switch table

|entry |open    |close    |read    |write    |ioctl    |
|:---  |---:    |---:     |---:    |---:     |---:     |
|0     |conopen |conclose |conread |conwrite |conioctl |
|1     |dzbopen |dzbclose |dzbread |dzbwrite |dzbioctl |
|2     |syopen  |nulldev  |syread  |sywrite  |syioctl  |
|3     |nulldev |nulldev  |mmread  |mmwrite  |nodev    |
|4     |gdopen  |gdclose  |gdread  |gdwrite  |nodev    |
|5     |gtopen  |gtclose  |gtread  |gtwrite  |nodev    |

##### 10.1.2 System Calls and the Driver Interface

1. open
2. close
3. read, write
4. strategy interface
5. ioctl
6. other file system related calls

##### 10.1.3 Interrupt Handlers

The kernel invokes the device specific intterrupt handler, passing it he device number or other parameters to identify the specific unit that caused the intterrupt.

#### 10.2 Disk Drivers

The disck driver translates a file system address, consisting of a logical device number and block number, to a particular sector on the disk.

Utility programs can use either the raw or block interface to access disk data directly, bypassing the regular file system access methods:

- `mkfs`: format a disk section for a UNIX file system, creating a super block, inode list, linked list of free disk blocks, and a root directory on the new files system.
- `fsck`: check the consistency of an existing file system and correct errors.

#### 10.3 Terminal Drivers
#### 10.4 Streams
#### 10.5 Summary

### 11. Interprocess Communication
#### 11.1 Process Tracing
#### 11.2 System V IPC
#### 11.3 Network Communications
#### 11.4 Sockets
#### 11.5 Summary

### 12. Multiprocessor Systems
#### 12.1 Problem of Multiprocessor Systems
#### 12.2 Solution With Master and Slave Processors
#### 12.3 Solution With Semaphores
#### 12.4 The Tunis System
#### 12.5 Performance Limitations

### 13. Distributed Unix Systems
#### 13.1 Satellite Processors
#### 13.2 The Newcastle Connection
#### 13.3 Transparent Distributed File Systems
#### 13.4 A Transparent Distributed Model Without Stub Processes
#### 13.5 System Calls
#### 13.6 Summary

## 总结

<!-- 概要记录书籍中如何解决关键性问题的. -->

## 应用

<!-- 记录如何使用书籍中方法论解决你自己的问题. -->

## 文献引用

<!-- 记录相关的和进一步阅读资料: 文献、网页链接等. -->

- Maurice J. Bach. **The Design of the UNIX Operating System**. Prentice Hall: 1986.

## 其他备注
