# Rust

- Home: https://www.rust-lang.org/

> A language empowering everyone to build reliable and efficient software.

- Learn Rust: https://www.rust-lang.org/learn

| Book                 | Description                                                                  |
| :------------------- | :--------------------------------------------------------------------------- |
| THE STANDARD LIBRARY | Comprehensive guide to the Rust standard library APIs.                       |
| EDITION GUIDE        | Guide to the Rust editions.                                                  |
| CARGO BOOK           | A book on Rust’s package manager and build system.                           |
| RUSTDOC BOOK         | Learn how to make awesome documentation for your crate.                      |
| RUSTC BOOK           | Familiarize yourself with the knobs available in the Rust compiler.          |
| COMPILER ERROR INDEX | In-depth explanations of the errors you may see from the Rust compiler.      |
|                      |                                                                              |
| COMMAND LINE BOOK    | Learn how to build effective command line applications in Rust.              |
| WEBASSEMBLY BOOK     | Use Rust to build browser-native libraries through WebAssembly.              |
| EMBEDDED BOOK        | Become proficient with Rust for Microcontrollers and other embedded systems. |

- Tools: https://www.rust-lang.org/tools

```
First-class editor support: 
VS CODE, SUBLIME TEXT, ATOM, INTELLIJ FAMILY, ECLIPSE, VIM, EMACS, GEANY

Bring calmness to your builds: 
Install, Test, Deploy

Velocity through automation:
Rustfmt, Clippy, Cargo Doc
```

Versions:

```shell
✗ rustup --version
rustup 1.26.0 (5af9b9484 2023-04-05)
info: This is the version for the rustup toolchain manager, not the rustc compiler.
info: The currently active `rustc` version is `rustc 1.70.0 (90c541806 2023-05-31)`
✗ rustup update

✗ cargo --version
cargo 1.70.0 (ec8a8a0ca 2023-04-25)
✗ rustc --version
rustc 1.70.0 (90c541806 2023-05-31)
✗ rustdoc --version
rustdoc 1.70.0 (90c541806 2023-05-31)
```

