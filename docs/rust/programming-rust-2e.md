# Notes of **Programming Rust, 2nd Edition**


| Date       | Description |
| :--------- | :---------- |
| 2023-06-24 | kick off.   |

| #    | Title                                                                                   | Progress | Description |
| :--- | :-------------------------------------------------------------------------------------- | :------- | :---------- |
| 1    | [Systems Programmers Can Have Nice Things](#1-systems-programmers-can-have-nice-things) | xxx%     | yyyy-mm-dd  |
| 2    | [A Tour of Rust](#2-a-tour-of-rust)                                                     | xxx%     | yyyy-mm-dd  |
| 3    | [Fundamental Types](#3-fundamental-types)                                               | xxx%     | yyyy-mm-dd  |
| 4    | [Ownership and Moves](#4-ownership-and-moves)                                           | xxx%     | yyyy-mm-dd  |
| 5    | [References](#5-references)                                                             | xxx%     | yyyy-mm-dd  |
| 6    | [Expressions](#6-expressions)                                                           | xxx%     | yyyy-mm-dd  |
| 7    | [Error Handling](#7-error-handling)                                                     | xxx%     | yyyy-mm-dd  |
| 8    | [Crates and Modules](#8-crates-and-modules)                                             | xxx%     | yyyy-mm-dd  |
| 9    | [Structs](#9-structs)                                                                   | xxx%     | yyyy-mm-dd  |
| 10   | [Enums and Patterns](#10-enums-and-patterns)                                            | xxx%     | yyyy-mm-dd  |
| 11   | [Traits and Generics](#11-traits-and-generics)                                          | xxx%     | yyyy-mm-dd  |
| 12   | [Operator Overloading](#12-operator-overloading)                                        | xxx%     | yyyy-mm-dd  |
| 13   | [Utility Traits](#13-utility-traits)                                                    | xxx%     | yyyy-mm-dd  |
| 14   | [Closures](#14-closures)                                                                | xxx%     | yyyy-mm-dd  |
| 15   | [Iterators](#15-iterators)                                                              | xxx%     | yyyy-mm-dd  |
| 16   | [Collections](#16-collections)                                                          | xxx%     | yyyy-mm-dd  |
| 17   | [Strings and Text](#17-strings-and-text)                                                | xxx%     | yyyy-mm-dd  |
| 18   | [Input and Output](#18-input-and-output)                                                | xxx%     | yyyy-mm-dd  |
| 19   | [Concurrency](#19-concurrency)                                                          | xxx%     | yyyy-mm-dd  |
| 20   | [Asynchronous Programming](#20-asynchronous-programming)                                | xxx%     | yyyy-mm-dd  |
| 21   | [Macros](#21-macros)                                                                    | xxx%     | yyyy-mm-dd  |
| 22   | [Unsafe Code](#22-unsafe-code)                                                          | xxx%     | yyyy-mm-dd  |
| 23   | [Foreign Functions](#23-foreign-functions)                                              | xxx%     | yyyy-mm-dd  |

## Tips for Recapture

<!-- 帮助重温的过程总结. -->

1. Step 1
2. Step 2
3. Step 3
4. Step 4

## 术语

<!-- 记录阅读过程中出现的关键字及其简单的解释. -->

<!-- 进展中需要再次确认的术语:

进行中: 术语1
已完成: ~~术语1~~
-->

## 介绍

<!-- 描述书籍阐述观点的来源、拟解决的关键性问题和采用的方法论等. -->

## 动机

<!-- 描述阅读书籍的动机, 要达到什么目的等. -->

1. 学习Rust的基本语法.
2. 考察Rust在并发安全方面的工作.

## 概念结构

<!-- 描述书籍的行文结构, 核心主题和子主题的内容结构和关系. -->

### 1. Systems Programmers Can Have Nice Things

--8<--
rust/programming-rust/01
--8<--

### 2. A Tour of Rust

--8<--
rust/programming-rust/02
--8<--

### 3. Fundamental Types

--8<--
rust/programming-rust/03
--8<--

### 4. Ownership and Moves

--8<--
rust/programming-rust/04
--8<--

### 5. References

--8<--
rust/programming-rust/05
--8<--

### 6. Expressions

--8<--
rust/programming-rust/06
--8<--

### 7. Error Handling

--8<--
rust/programming-rust/07
--8<--

### 8. Crates and Modules

--8<--
rust/programming-rust/08
--8<--

### 9. Structs

--8<--
rust/programming-rust/09
--8<--

### 10. Enums and Patterns

--8<--
rust/programming-rust/10
--8<--

### 11. Traits and Generics

--8<--
rust/programming-rust/11
--8<--

### 12. Operator Overloading

--8<--
rust/programming-rust/12
--8<--

### 13. Utility Traits

--8<--
rust/programming-rust/13
--8<--

### 14. Closures

--8<--
rust/programming-rust/14
--8<--

### 15. Iterators

--8<--
rust/programming-rust/15
--8<--

### 16. Collections

--8<--
rust/programming-rust/16
--8<--

### 17. Strings and Text

--8<--
rust/programming-rust/17
--8<--

### 18. Input and Output

--8<--
rust/programming-rust/18
--8<--

### 19. Concurrency

--8<--
rust/programming-rust/19
--8<--

### 20. Asynchronous Programming

--8<--
rust/programming-rust/20
--8<--

### 21. Macros

--8<--
rust/programming-rust/21
--8<--

### 22. Unsafe Code

--8<--
rust/programming-rust/22
--8<--

### 23. Foreign Functions

--8<--
rust/programming-rust/23
--8<--

## 总结

<!-- 概要记录书籍中如何解决关键性问题的. -->

## 应用

<!-- 记录如何使用书籍中方法论解决你自己的问题. -->

## 文献引用

<!-- 记录相关的和进一步阅读资料: 文献、网页链接等. -->

- Jim Blandy, Jason Orendorff, Leonora F.S. Tindall. **Programming Rust, 2nd Edition**. O’Reilly Media: 2021.

## 其他备注
