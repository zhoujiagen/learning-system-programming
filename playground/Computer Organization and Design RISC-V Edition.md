# Notes of **Computer Organization and Design RISC-V Edition**


|时间       |内容      |
|:---       |:---      |
|2022-07-11 |kick off. |

## 术语

<!-- 记录阅读过程中出现的关键字及其简单的解释. -->

<!-- 进展中需要再次确认的术语:

进行中: 术语1
已完成: ~~术语1~~
-->

## 介绍

<!-- 描述书籍阐述观点的来源、拟解决的关键性问题和采用的方法论等. -->

## 动机

<!-- 描述阅读书籍的动机, 要达到什么目的等. -->

1. 熟悉计算机体系结构中的概念: 指令、算术操作、处理器、内存层次、并行处理器.
2. 熟悉常见的RISC-V指令, 并编写简单的C语言结构对应的汇编程序.
3. 了解计算机系统中常见的性能瓶颈和优化方法.
4. 了解: 逻辑设计、GPU、硬件控制映射、常见的RISC体系结构.

## 概念结构

<!-- 描述书籍的行文结构, 核心主题和子主题的内容结构和关系. -->

### 1. Computer Abstractions and Technology
#### 1.1 Introduction
#### 1.2 Eight Great Ideas in Computer Architecture
#### 1.3 Below Your Program
#### 1.4 Under the Covers
#### 1.5 Technologies for Building Processors and Memory
#### 1.6 Performance
#### 1.7 The Power Wall
#### 1.8 The Sea Change: The Switch from Uniprocessors to Multiprocessors
#### 1.9 Real Stuff: Benchmarking the Intel Core i7
#### 1.10 Fallacies and Pitfalls
#### 1.11 Concluding Remarks
#### 1.12 Historical Perspective and Further Reading
#### 1.13 Exercises

### 2. Instructions: Language of the Computer
#### 2.1 Introduction
#### 2.2 Operations of the Computer Hardware
#### 2.3 Operands of the Computer Hardware
#### 2.4 Signed and Unsigned Numbers
#### 2.5 Representing Instructions in the Computer
#### 2.6 Logical Operations
#### 2.7 Instructions for Making Decisions
#### 2.8 Supporting Procedures in Computer Hardware
#### 2.9 Communicating with People
#### 2.10 RISC-V Addressing for Wide Immediates and Addresses
#### 2.11 Parallelism and Instructions: Synchronization
#### 2.12 Translating and Starting a Program
#### 2.13 A C Sort Example to Put it All Together
#### 2.14 Arrays versus Pointers
#### 2.15 Advanced Material: Compiling C and Interpreting Java
#### 2.16 Real Stuff: MIPS Instructions
#### 2.17 Real Stuff: x86 Instructions
#### 2.18 Real Stuff: The Rest of the RISC-V Instruction Set
#### 2.19 Fallacies and Pitfalls
#### 2.20 Concluding Remarks
#### 2.21 Historical Perspective and Further Reading
#### 2.22 Exercises

### 3. Arithmetic for Computers
#### 3.1 Introduction
#### 3.2 Addition and Subtraction
#### 3.3 Multiplication
#### 3.4 Division
#### 3.5 Floating Point
#### 3.6 Parallelism and Computer Arithmetic: Subword Parallelism
#### 3.7 Real Stuff: Streaming SIMD Extensions and Advanced Vector Extensions in x86
#### 3.8 Going Faster: Subword Parallelism and Matrix Multiply
#### 3.9 Fallacies and Pitfalls
#### 3.10 Concluding Remarks
#### 3.11 Historical Perspective and Further Reading
#### 3.12 Exercises

### 4. The Processor
#### 4.1 Introduction
#### 4.2 Logic Design Conventions
#### 4.3 Building a Datapath
#### 4.4 A Simple Implementation Scheme
#### 4.5 An Overview of Pipelining
#### 4.6 Pipelined Datapath and Control
#### 4.7 Data Hazards: Forwarding versus Stalling
#### 4.8 Control Hazards
#### 4.9 Exceptions
#### 4.10 Parallelism via Instructions
#### 4.11 Real Stuff: The ARM Cortex-A53 and Intel Core i7 Pipelines
#### 4.12 Going Faster: Instruction-Level Parallelism and Matrix Multiply
#### 4.13 Advanced Topic: An Introduction to Digital Design Using a Hardware Design Language to Describe and Model a Pipeline and More Pipelining Illustrations
#### 4.14 Fallacies and Pitfalls
#### 4.15 Concluding Remarks
#### 4.16 Historical Perspective and Further Reading
#### 4.17 Exercises

### 5. Large and Fast: Exploiting Memory Hierarchy
#### 5.1 Introduction
#### 5.2 Memory Technologies
#### 5.3 The Basics of Caches
#### 5.4 Measuring and Improving Cache Performance
#### 5.5 Dependable Memory Hierarchy
#### 5.6 Virtual Machines
#### 5.7 Virtual Memory
#### 5.8 A Common Framework for Memory Hierarchy
#### 5.9 Using a Finite-State Machine to Control a Simple Cache
#### 5.10 Parallelism and Memory Hierarchy: Cache Coherence
#### 5.11 Parallelism and Memory Hierarchy: Redundant Arrays of Inexpensive Disks
#### 5.12 Advanced Material: Implementing Cache Controllers
#### 5.13 Real Stuff: The ARM Cortex-A53 and Intel Core i7 Memory Hierarchies
#### 5.14 Real Stuff: The Rest of the RISC-V System and Special Instructions
#### 5.15 Going Faster: Cache Blocking and Matrix Multiply
#### 5.16 Fallacies and Pitfalls
#### 5.17 Concluding Remarks
#### 5.18 Historical Perspective and Further Reading
#### 5.19 Exercises

### 6. Parallel Processors from Client to Cloud
#### 6.1 Introduction
#### 6.2 The Difficulty of Creating Parallel Processing Programs
#### 6.3 SISD, MIMD, SIMD, SPMD, and Vector
#### 6.4 Hardware Multithreading
#### 6.5 Multicore and Other Shared Memory Multiprocessors
#### 6.6 Introduction to Graphics Processing Units
#### 6.7 Clusters, Warehouse Scale Computers, and Other Message-Passing Multiprocessors
#### 6.8 Introduction to Multiprocessor Network Topologies
#### 6.9 Communicating to the Outside World: Cluster Networking
#### 6.10 Multiprocessor Benchmarks and Performance Models
#### 6.11 Real Stuff: Benchmarking and Rooflines of the Intel Core i7 960 and the NVIDIA Tesla GPU
#### 6.12 Going Faster: Multiple Processors and Matrix Multiply
#### 6.13 Fallacies and Pitfalls
#### 6.14 Concluding Remarks
#### 6.15 Historical Perspective and Further Reading
#### 6.16 Exercises

### A. The Basics of Logic Design
#### A.1 Introduction
#### A.2 Gates, Truth Tables, and Logic Equations
#### A.3 Combinational Logic
#### A.4 Using a Hardware Description Language
#### A.5 Constructing a Basic Arithmetic Logic Unit
#### A.6 Faster Addition: Carry Lookahead
#### A.7 Clocks
#### A.8 Memory Elements: Flip-Flops, Latches, and Registers
#### A.9 Memory Elements: SRAMs and DRAMs
#### A.10 Finite-State Machines
#### A.11 Timing Methodologies
#### A.12 Field Programmable Devices
#### A.13 Concluding Remarks
#### A.14 Exercises

### B. Graphics and Computing GPUs
#### B.1 Introduction
#### B.2 GPU System Architectures
#### B.3 Programming GPUs
#### B.4 Multithreaded Multiprocessor Architecture
#### B.5 Parallel Memory System
#### B.6 Floating-point Arithmetic
#### B.7 Real Stuff: The NVIDIA GeForce 8800
#### B.8 Real Stuff: Mapping Applications to GPUs
#### B.9 Fallacies and Pitfalls
#### B.10 Concluding Remarks
#### B.11 Historical Perspective and Further Reading

### C. Mapping Control to Hardware
#### C.1 Introduction
#### C.2 Implementing Combinational Control Units
#### C.3 Implementing Finite-State Machine Control
#### C.4 Implementing the Next-State Function with a Sequencer
#### C.5 Translating a Microprogram to Hardware
#### C.6 Concluding Remarks
#### C.7 Exercises

### D. A Survey of RISC Architectures for Desktop, Server, and Embedded Computers
#### D.1 Introduction
#### D.2 Addressing Modes and Instruction Formats
#### D.3 Instructions: The MIPS Core Subset
#### D.4 Instructions: Multimedia Extensions of the Desktop/Server RISCs
#### D.5 Instructions: Digital Signal-Processing Extensions of the Embedded RISCs
#### D.6 Instructions: Common Extensions to MIPS Core
#### D.7 Instructions Unique to MIPS-64
#### D.8 Instructions Unique to Alpha
#### D.9 Instructions Unique to SPARC v9
#### D.10 Instructions Unique to PowerPC
#### D.11 Instructions Unique to PA-RISC 2.0
#### D.12 Instructions Unique to ARM
#### D.13 Instructions Unique to Thumb
#### D.14 Instructions Unique to SuperH
#### D.15 Instructions Unique to M32R
#### D.16 Instructions Unique to MIPS-16
#### D.17 Concluding Remarks

## 总结

<!-- 概要记录书籍中如何解决关键性问题的. -->

## 应用

<!-- 记录如何使用书籍中方法论解决你自己的问题. -->

## 文献引用

<!-- 记录相关的和进一步阅读资料: 文献、网页链接等. -->

- David Patterson, John Hennessy. **Computer Organization and Design RISC-V Edition: The Hardware Software Interface**. Morgan Kaufmann: 2017.

## 其他备注


- online materials: https://www.elsevier.com/books-and-journals/book-companion/9780128122754
- download: http://home.ustc.edu.cn/~louwenqi/reference_books_tools/Computer%20Organization%20and%20Design%20RISC-V%20edition.pdf

- Verilog教程: https://www.runoob.com/w3cnote/verilog-tutorial.html
- Verilog仿真工具iverilog+GTKWave: https://zhuanlan.zhihu.com/p/95081329

- Synthesis vs Simulation in VHDL: https://buzztech.in/synthesis-vs-simulation-in-vhdl/

